<?php

class ItemsWriter
{
    protected $items = [];

    public function addItem(Writable $items)
    {
        $this->items[] = $items;
    }

    public function write()
    {
        $sum = '';

        foreach ($this->items as $item)
        {
            $sum .= $item->getSummeryLine() . '<br>';
        }

        return $sum;

    }

}