<?php

class Application
{
    public static $db;

    public static function init()
    {
        try {
            self::$db = new PDO(
                'mysql:host=localhost;dbname=home17',
                'user',
                '123456'
            );

        } catch(Exception $e) {
            echo 'Cannot create connection';
            die();
        }
    }

    public function getProduct()
    {
        $sql = 'select * from products';

        $query = self::$db->prepare($sql);
        $query->execute();
        $products = $query->fetchAll();

        return $products;
    }
}