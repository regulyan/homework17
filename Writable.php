<?php
	
interface Writable
{
	public function getSummeryLine();
}