<?php

require_once 'Writable.php';

abstract class Item implements Writable
{
    protected $title;
    protected $type;
    protected $price;

    public function __construct($title, $price, $type)
    {
        $this->title = $title;
        $this->price = $price;
        $this->type = $type;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public static function getType()
    {
        return 'base_type';
    }

    abstract public function getPrice();

    public function getSummeryLine()
    {
        return $this->title; // TODO: Implement getSummeryLine() method.
    }
}