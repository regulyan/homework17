<?php

ini_set('display_errors', 1);

require_once 'Application.php';
require_once 'array.php';

Application::init();
$db = Application::$db;

try {

$sql = 'CREATE TABLE products (
	id int not null auto_increment,
	title varchar(255),
	p_type varchar(255),
	price varchar(255),
	discount varchar(255),
	description varchar(255),
	primary key (id)
);';

$db->exec($sql);

foreach ($array as $arr) {
	switch ($arr['p_type']) {
		case 'Goods':
			$sql = 'insert into products set
				title = :title,
				p_type = :p_type,
				price = :price,
				discount = :discount,
				description = :description
				';
			$s = $db->prepare($sql);
			$s->bindValue(':title', $arr['title']);
			$s->bindValue(':p_type', $arr['p_type']);
			$s->bindValue(':price', $arr['price']);
			$s->bindValue(':discount', $arr['discount']);
			$s->bindValue(':description', $arr['description']);
			break;			
		case 'Food':
			$sql = 'insert into products set
				title = :title,
				p_type = :p_type,
				price = :price,
				discount = :discount,
				description = :description
				';
			$s = $db->prepare($sql);
            $s->bindValue(':title', $arr['title']);
            $s->bindValue(':p_type', $arr['p_type']);
            $s->bindValue(':price', $arr['price']);
            $s->bindValue(':discount', $arr['discount']);
            $s->bindValue(':description', $arr['description']);
			break;
        case 'Bonus':
            $sql = 'insert into products set
				title = :title,
				p_type = :p_type,
				price = :price,
				discount = :discount,
				description = :description
				';
            $s = $db->prepare($sql);
            $s->bindValue(':title', $arr['title']);
            $s->bindValue(':p_type', $arr['p_type']);
            $s->bindValue(':price', $arr['price']);
            $s->bindValue(':discount', $arr['discount']);
            $s->bindValue(':description', $arr['description']);
            break;
	}

	$s->execute();
}

echo 'Migrate success';

} catch(Exception $e) {
	echo $e->getMessege();
}