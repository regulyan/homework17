<?php

ini_set('display_errors', 1);

require_once 'Application.php';
require_once 'Item.php';
require_once 'Writable.php';
require_once 'Food.php';
require_once 'GoodsItem.php';
require_once 'Bonus.php';
require_once 'ItemsWriter.php';

Application::init();
$db = new Application();
$products = $db->getProduct();

//echo '<pre>';
//print_r($products);
//echo '<br>';

$ItemsWriter = new ItemsWriter();

foreach ($products as $product){
    switch ($product['p_type']){
        case 'Goods':
            $item = new GoodsItem($product['title'], $product['price'], $product['p_type'], $product['discount']);
            break;
        case 'Food':
            $item = new Food($product['title'], $product['price'], $product['p_type']);
            break;
        case 'Bonus':
            $item = new Bonus($product['title'], $product['p_type'], $product['description']);
            break;
    }
    $ItemsWriter->addItem($item);
}

//echo $ItemsWriter->write();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Дом задание 17</title>
    <meta name="description" content="" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="container">
    <?php include 'header.php' ?>

    <div id="main">
        <div class="content">
            <h1>Goods & Foods</h1>
            <?php echo $ItemsWriter->write();?>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>

</body>
</html>

