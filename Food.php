<?php

class Food extends Item
{
    public static function getType()
    {
        return 'Food';
    }

    public function getPrice()
    {
        return $this->price - 10; // TODO: Implement getPrice() method.
    }

    public function getSummeryLine()
    {
        return parent::getSummeryLine() . ' - ' . static::getType() . ' - ' .  $this->getPrice(); // TODO: Change the autogenerated stub
    }

}