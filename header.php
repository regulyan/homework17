<div id="header">
	<div class="row">
		<div class="col-6 col-sm-8 col-md-8 col-lg-8 navmenu">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php">Home</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>

		<div class="col-6 col-sm-4 col-md-4 col-lg-4 logo">
			<div class="logo-img">
				<a class="header-logo" href="index.php"><img src="logo.jpg" title="" alt=""></a>
			</div>
		</div>
	</div>
</div>