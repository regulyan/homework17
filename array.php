<?php

$array = [
	[ 
		'title' => 'Water',
		'p_type' => 'Goods',
		'price' =>'20',
		'discount' => '5',
		'description' => '',
	],
	[
        'title' => 'Beer',
        'p_type' => 'Goods',
        'price' =>'90',
        'discount' => '3',
        'description' => '',
	],
	[
        'title' => 'Milka',
        'p_type' => 'Food',
        'price' =>'30',
        'discount' => '',
        'description' => '',
	],
	[
        'title' => 'Kinder',
        'p_type' => 'Food',
        'price' =>'28',
        'discount' => '',
        'description' => '',
	],
    [
        'title' => 'Pen',
        'p_type' => 'Bonus',
        'price' =>'',
        'discount' => '',
        'description' => 'Pen description',
    ],
    [
        'title' => 'Pencil',
        'p_type' => 'Bonus',
        'price' =>'',
        'discount' => '',
        'description' => 'Pencil description',
    ]
];